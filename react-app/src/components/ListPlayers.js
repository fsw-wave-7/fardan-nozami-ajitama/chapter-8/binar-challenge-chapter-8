import React from 'react';
import { Col, Table } from 'react-bootstrap';
import Player from './Player';

const ListPlayers = ({ players, _editData,  }) => (
  <Col>
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>No</th>
          <th>Username</th>
          <th>Email</th>
          <th>Experience</th>
          <th>Level</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {players.map((onePlayer, index) => (
          <Player
            key={onePlayer.id}
            player={onePlayer}
            index={index}
            _editData={_editData}
          />
        ))}
      </tbody>
    </Table>
  </Col>
);

export default ListPlayers;
