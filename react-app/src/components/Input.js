import React from 'react';

const Input = ({ _saveTypeFilter }) => (
  <div>
    <div>
      <label>Type Filter : </label>
      <select onChange={(event) => _saveTypeFilter(event)}>
        <option value="none">None</option>
        <option value="username">Username</option>
        <option value="email">Email</option>
        <option value="exp">Exp</option>
        <option value="level">Level</option>
      </select>
    </div>
  </div>
);

export default Input;
