import React from 'react';
import { Button } from 'react-bootstrap';

const Player = ({ player, index, _editData }) => (
  <tr>
    <td>{index + 1}</td>
    <td>{player.username}</td>
    <td>{player.email}</td>
    <td>{player.exp}</td>
    <td>{player.level}</td>
    <td>
      <Button
        onClick={() => _editData(player.id)}
        variant="primary"
        type="submit"
      >
        Edit
      </Button>
    </td>
  </tr>
);

export default Player;
