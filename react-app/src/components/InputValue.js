import React from 'react';

const InputUsername = ({ _changeUsername }) => (
  <div>
    <label>username : </label>
    <input
      type="text"
      name="username"
      onChange={(event) => _changeUsername(event.target.value)}
    />
  </div>
);

const InputEmail = ({ _changeEmail }) => (
  <div>
    <label>Email: </label>
    <input
      type="email"
      name="Email"
      onChange={(event) => _changeEmail(event.target.value)}
    />
  </div>
);

const InputExp = ({ _changeExp }) => (
  <div>
    <label>Exp : </label>
    <input
      type="number"
      name="exp"
      onChange={(event) => _changeExp(event.target.value)}
    />
  </div>
);

const InputLevel = ({ _changeLevel }) => (
  <div>
    <label>Level : </label>
    <input
      type="number"
      name="level"
      onChange={(event) => _changeLevel(event.target.value)}
    />
  </div>
);

const InputValue = ({
  tipe,
  _changeUsername,
  _changeEmail,
  _changeExp,
  _changeLevel,
}) => {
  switch (tipe) {
    case 'username':
      return <InputUsername _changeUsername={_changeUsername}></InputUsername>;
    case 'email':
      return <InputEmail _changeEmail={_changeEmail}></InputEmail>;
    case 'exp':
      return <InputExp _changeExp={_changeExp}></InputExp>;
    case 'level':
      return <InputLevel _changeLevel={_changeLevel}></InputLevel>;
    case 'none':
      return null;
    default:
      return null;
  }
};

export default InputValue;
