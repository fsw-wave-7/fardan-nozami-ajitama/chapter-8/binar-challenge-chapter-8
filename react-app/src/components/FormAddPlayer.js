import React from 'react';
import { Button, Col, Form } from 'react-bootstrap';

const FormAddPlayer = ({
  username,
  email,
  password,
  exp,
  level,
  submitData,
  handleChange,
}) => (
  <Col>
    <Form onSubmit={submitData}>
      <Form.Group className="mb-3" controlId="username">
        <Form.Label>Username</Form.Label>
        <Form.Control
          type="text"
          placeholder="Input your username"
          value={username}
          onChange={(event) => handleChange('username', event.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="email">
        <Form.Label>Email</Form.Label>
        <Form.Control
          type="email"
          placeholder="Input your email"
          value={email}
          onChange={(event) => handleChange('email', event.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Input your password"
          value={password}
          onChange={(event) => handleChange('password', event.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="exp">
        <Form.Label>Exp</Form.Label>
        <Form.Control
          type="number"
          placeholder="Input your expe"
          value={exp}
          onChange={(event) => handleChange('exp', event.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="level">
        <Form.Label>Level</Form.Label>
        <Form.Control
          type="number"
          placeholder="Input your level"
          value={level}
          onChange={(event) => handleChange('level', event.target.value)}
        />
      </Form.Group>

      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
  </Col>
);

export default FormAddPlayer;
