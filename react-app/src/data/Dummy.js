const Dummy = [
  {
    id: 1,
    username: 'Boni',
    email: 'Saputro@gmail.com',
    password: '123',
    exp: 1800,
    level: 1,
  },
  {
    id: 2,
    username: 'Andi',
    email: 'Danca@gmail.com',
    password: '234',
    exp: 500,
    level: 0,
  },
];

export default Dummy;
