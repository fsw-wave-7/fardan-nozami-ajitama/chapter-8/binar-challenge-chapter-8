import React, { Component, Fragment } from 'react';
import { Container, Row } from 'react-bootstrap';
import FormAddPlayer from '../components/FormAddPlayer';
import ListPlayer from '../components/ListPlayers';
import Dummy from '../data/Dummy';
import Input from '../components/Input';
import InputValue from '../components/InputValue';

export default class Player extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: null,
      tipe: 'none',
      username: '',
      usernameSearch: '',
      email: '',
      emailSearch: '',
      password: '',
      exp: '',
      expSearch: '',
      level: '',
      levelSearch: '',
      players: Dummy,
    };
  }

  _handleChange = (key, value) => {
    this.setState({
      [key]: value,
    });
  };

  _editData = (id) => {
    const { players } = this.state;
    const selectedPlayer = players.filter((player) => player.id === id);
    this.setState({
      username: selectedPlayer[0].username,
      email: selectedPlayer[0].email,
      exp: selectedPlayer[0].exp,
      level: selectedPlayer[0].level,
      id,
    });
  };

  _submitData = (event) => {
    event.preventDefault();
    const { players, username, email, exp, level, id, password } = this.state; // untuk ambil value dari state
    if (id) {
      const newPlayers = players.filter((player) => player.id !== id);
      newPlayers.push({ id, username, email, exp, level, password }); // masukkan data baru
      this.setState({ players: newPlayers }); // update state
      this.setState({
        username: '',
        email: '',
        password: '',
        exp: '',
        level: '',
      }); //kosongkan form
    } else {
      players.push({
        id: players.length + 1,
        username,
        password,
        email,
        exp,
        level,
      }); // masukkan data baru
      this.setState({ players }); // update state
      this.setState({
        username: '',
        email: '',
        password: '',
        exp: '',
        level: '',
      }); //kosongkan form
    }
  };

  _changeUsername = (value) => this.setState({ usernameSearch: value });

  _changeEmail = (value) => this.setState({ emailSearch: value });
  _changeExp = (value) => this.setState({ expSearch: value });
  _changeLevel = (value) => this.setState({ levelSearch: value });
  _changeNone = (value) => this.setState({ none: value });
  _saveTypeFilter = (event) => this.setState({ tipe: event.target.value });

  _filterPlayers = () => {
    const {
      players,
      usernameSearch,
      emailSearch,
      expSearch,
      levelSearch,
      tipe,
    } = this.state;
    if (players && players.length > 0) {
      switch (tipe) {
        case 'username':
          return players.filter((player) =>
            player.username.toLowerCase().includes(usernameSearch.toLowerCase())
          );
        case 'email':
          return players.filter((player) =>
            player.email.toLowerCase().includes(emailSearch.toLowerCase())
          );
        case 'exp':
          return players.filter((player) => player.exp === parseInt(expSearch));
        case 'level':
          return players.filter(
            (player) => player.level === parseInt(levelSearch)
          );

        case 'none':
          return players;

        default:
          return players;
      }
    } else {
      return [];
    }
  };

  render() {
    const { username, email, exp, level, tipe, password } = this.state;
    return (
      <Fragment>
        <Container className="p-2 mt-5">
          <Input _saveTypeFilter={this._saveTypeFilter} />
          <InputValue
            tipe={tipe}
            _changeUsername={this._changeUsername}
            _changeEmail={this._changeEmail}
            _changeExp={this._changeExp}
            _changeLevel={this._changeLevel}
            _changeNone={this._changeNone}
            _handleChange={this._handleChange}
          />
          <Row>
            <ListPlayer
              players={this._filterPlayers()}
              _editData={this._editData}
            />
            <FormAddPlayer
              username={username}
              email={email}
              password={password}
              exp={exp}
              level={level}
              submitData={this._submitData}
              handleChange={this._handleChange}
            />
          </Row>
        </Container>
      </Fragment>
    );
  }
}
