import React, { Component, Fragment } from 'react';
import { Col, Container, Row, Table, Form, Button } from 'react-bootstrap';

export default class Player extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [
        {
          id: 1,
          username: 'Boni',
          email: 'Saputro@gmail.com',
          experience: 1800,
          level: 1,
        },
        {
          id: 2,
          username: 'Andi',
          email: 'Danca@gmail.com',
          experience: 500,
          level: 0,
        },
      ],
      id: null,
      username: '',
      email: '',
      experience: '',
      level: '',
    };
  }

  handleChange = (key, value) => {
    console.log(key, value);
    this.setState({
      [key]: value,
    });
  };

  editData = (id) => {
    const playerChosen = this.state.data
      .filter((player) => player.id === id)
      .map((playerFilter) => {
        return playerFilter;
      });
    this.setState({
      id: playerChosen[0].id,
      username: playerChosen[0].username,
      email: playerChosen[0].email,
      experience: playerChosen[0].experience,
      level: playerChosen[0].level,
    });
  };
  submitData = (event) => {
    event.preventDefault();
    const { data, username, email, experience, level, id } = this.state; // untuk ambil value dari state

    if (id) {
      const playerNotChosen = data
        .filter((player) => player.id !== id)
        .map((playerFilter) => {
          return playerFilter;
        });

      this.setState({
        data: [
          ...playerNotChosen,
          {
            id,
            username,
            email,
            experience,
            level,
          },
        ],
      });
    } else {
      this.setState({
        data: [
          ...data,
          {
            id: data.length + 1,
            username,
            email,
            experience,
            level,
          },
        ],
      }); // update state
    }
  };

  render() {
    const { data, username, email, experience, level } = this.state;
    console.log(data);

    return (
      <Fragment>
        <Container className="p-2 mt-5">
          <Row>
            <Col>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Experience</th>
                    <th>Level</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((player, index) => {
                    return (
                      <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{player.username}</td>
                        <td>{player.email}</td>
                        <td>{player.experience}</td>
                        <td>{player.level}</td>
                        <td>
                          <Button
                            onClick={() => this.editData(player.id)}
                            variant="primary"
                            type="submit"
                          >
                            Edit
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </Col>
            <Col>
              <Form onSubmit={this.submitData}>
                <Form.Group className="mb-3" controlId="username">
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Input your username"
                    value={username}
                    onChange={(event) =>
                      this.handleChange('username', event.target.value)
                    }
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="email">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Input your email"
                    value={email}
                    onChange={(event) =>
                      this.handleChange('email', event.target.value)
                    }
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="experience">
                  <Form.Label>Experience</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Input your experience"
                    value={experience}
                    onChange={(event) =>
                      this.handleChange('experience', event.target.value)
                    }
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="level">
                  <Form.Label>Level</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Input your level"
                    value={level}
                    onChange={(event) =>
                      this.handleChange('level', event.target.value)
                    }
                  />
                </Form.Group>

                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
      </Fragment>
    );
  }
}
